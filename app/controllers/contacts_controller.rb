class ContactsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_contact, except: [:index, :new, :create, :autocomplete]

  def index
    session[:selected_group_id] = params[:group_id]
    if params[:group_id] && !params[:group_id].empty?
      # @contacts = Contact.where(group_id: params[:group_id]).page(params[:page])
      group = Group.find(params[:group_id])
      if params[:term] && !params[:term].empty?
        @contacts = group.current_user.contacts.where('name LIKE ?', "%#{params[:term]}%").order(created_at: :desc).page(params[:page])
      else
        @contacts = group.contacts.order(created_at: :desc).page(params[:page])
      end
    else
      @contacts = Contact.where('name LIKE ?', "%#{params[:term]}%").order(created_at: :desc).page(params[:page])
    end
  end

  def autocomplete
    @contacts = current_user.contacts.search(params[:term]).order(created_at: :desc).page(params[:page])
    render json: @contacts.map { |contact| { id: contact.id, value: contact.name } }
  end

  def new
    @contact = Contact.new
  end

  def create
    @contact = current_user.contacts.build(contact_params)
    if @contact.save
      flash[:success] = "#{@contact.name} was created in your contacts"
      redirect_to contacts_path(previous_query_string)
    else
      flash[:error] = "Contact failed to be created!"
      render :new
    end
  end

  def edit
    authorize @contact
  end

  def update
    authorize @contact
    if @contact.update(contact_params)
      flash[:success] = "Contact was successfully updated!"
      redirect_to contacts_path(previous_query_string)
    else
      flash[:error] = 'Contact failed to be updated!'
      render :edit
    end
  end

  def destroy
    authorize @contact
    if @contact.destroy
      flash[:danger] = "#{@contact.name} was deleted!"
      redirect_to root_url
    end
  end

  private

  def contact_params
    params.require(:contact).permit(:name, :email, :company, :phone, :address, :group_id, :avatar)
  end

  def set_contact
    @contact = Contact.find(params[:id])
  end

  def previous_query_string
    session[:selected_group_id] ? { group_id: session[:selected_group_id] } : {}
  end
end
