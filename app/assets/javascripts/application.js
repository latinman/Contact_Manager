// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require jquery
//= require jquery-ui
//= require bootstrap/dropdown
//= require jasny-bootstrap.min
//= require activestorage
//= require turbolinks
//= require toastr
//= require_tree .

$(document).on('turbolinks:load', function() {
  $(function() {
  $('#term').autocomplete({
    source: '/contacts/autocomplete',
    minLength: 3,
    select: function(event, ui) {
      $('#term').val(ui.item.value);
      $(this).closest('form').submit();
    }
  });
  $('#add-new-group').hide();
  $('#add-group-btn').click(function() {
    $('#add-new-group').slideToggle(function() {
      $('#new-group').focus();
    });
    return false;
  });

  $('#save-group-btn').click(function(event) {
    event.preventDefault();

    var newGroup = $('#new_group');
    var inputGroup = newGroup.closest('.input-group');

    $.ajax({
      url: '/groups',
      method: 'post',
      data: {
         group: { name: $('#new_group').val() }
      },
      success: function(group) {
        if (group_id != null) {
          inputGroup.removeClass('has-error');
          inputGroup.next('text-danger').remove();

          var newOption = $('<option />')
                .attr('value', group.id)
                .attr('selected', true)
                .text(group.name);
          $('#contact_group_id').append(newOption);

          newGroup.val('');
        }
      },
      error: function(xhr) {
        var errors = xhr.responseJSON;
        var error = errors.join(", ");
        if (error) {

          inputGroup.next('.text-danger').detach();
          inputGroup
            .addClass('has-error')
            .after('<p class="text-danger">' + error + '</p>');
        }
      }
    });
  });
});

  toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-top-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
  }
});

$(document).on('click', '.pagination a[data-remote=true], a.list-group-item', function() {
  history.pushState({}, '', $(this).attr('href'));
});

$(window).on('popstate', function() {
  $.get(document.location.href);
});
