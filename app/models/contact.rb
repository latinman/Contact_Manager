class Contact < ApplicationRecord
  belongs_to :group
  belongs_to :user

  paginates_per 10
  validates :name, :email, :group_id, presence: true
  validates :name, length: { minimum: 2 }

  has_one_attached :avatar

  scope :search, -> (term) do
    where('LOWER(name) LIKE :term or LOWER(company) LIKE :term or LOWER(email LIKE :term)', term: "%#{term.downcase}%") if term.present?
  end

  def gravatar
    hash = Digest::MD5.hexdigest(email.downcase)
     "https://www.gravatar.com/avatar/#{hash}"
  end
end
